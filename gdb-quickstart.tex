\documentclass[10pt,a4paper]{article}

\usepackage{fullpage}
\usepackage[brazilian]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{indentfirst}

\usepackage{graphicx}
\usepackage{array}
\usepackage{subfigure}
\usepackage{float}
\usepackage{color}
\usepackage{hyperref}
\hypersetup{
    pdftitle={Uso do GDB no ARM (MC404)},
    pdfauthor={Alexandre Medeiros},
    hidelinks
}

\title{Uso do GDB na infraestrutura ARM para MC404}
\author{Alexandre Medeiros}
\date{\today}

\begin{document}
\maketitle

\section{Introdução - O que é o GDB?}
GDB, o depurador do projeto GNU, é um programa cujo objetivo é localizar bugs em
outros programas, para isso ele possui diversas funcionalidades, como pausar a
execução do programa em determinados pontos, examinar o valor de variáveis e até
mesmo executar o programa instrução por instrução.

\begin{figure}[h]
    \centering
    \includegraphics{imgs/gdb-logo}
    \caption{Logo do GDB}
\end{figure}

\section{Como usar?}
Antes de utilizar o GDB é necessário compilar o seu programa com as informações
de depuração necessárias para que o GDB execute-o corretamente, disponibilizando
informações sobre as variáveis, rótulos e instruções que estão sendo executadas.

Para isso, ao montar e ligar seu programa, basta adicionar a flag {\tt -g},
logo, os comandos ficarão:

\begin{verbatim}
$ as arquivo.s -g -o arquivo.o
\end{verbatim}

\begin{verbatim}
$ gcc arquivo1.o [arquivo2.o ...] -g -o programa
\end{verbatim}

Agora, para utilizar o GDB para depurar seu programa, basta executar o comando:

\begin{verbatim}
$ gdb ./programa
\end{verbatim}

\section{Como depurar?}
Agora que conseguimos abrir nosso programa no GDB, o que podemos fazer de bom?
A princípio, seu programa pode ser executado normalmente no GDB, basta usar o
comando:

\begin{verbatim}
(gdb) run [argumentos]
\end{verbatim}

Com isso seu programa será executado com os argumentos passados para o comando
{\tt run}, note que esses argumentos são os mesmos utilizados para executar seu
programa no terminal, como em
\begin{verbatim}
$ ./programa arquivo_entrada.txt > saida.txt
\end{verbatim}

Usando apenas o {\tt run} seu programa será executado até o fim, seja esse fim o
fim de execução normal ({\tt return 0;} na main) ou um erro como o temido
Segmentation Fault.

No caso de um erro como o SegFault o GDB irá indicar em qual linha do seu código
que aconteceu esse problema e lhe permitirá examinar os valores das
variáveis nesse ponto.

\subsection{Pausando a execução}
Uma das principais funcionalidades do GDB é ter a capacidade de pausar a
execução do seu programa em determinados pontos, esses pontos são conhecidos
como breakpoints. %e Watchpoints.

\subsubsection{Breakpoints}
Um breakpoint nada mais é do que um ponto no código que, antes de ser executado,
o GDB pausa a execução e disponibiliza seu terminal para que você possa
inspecionar a situação atual do seu programa.

Há diversas maneiras de se criar um breakpoint, você pode criá-lo a partir de
uma linha do seu arquivo fonte, por exemplo, você deseja que o programa pause na
instrução que está na linha 7 do arquivo main.s, você deve usar o comando
\begin{verbatim}
(gdb) break main.s:7
\end{verbatim}

Também podemos criar um breakpoint numa função. Considere que queremos criar um
breakpoint na função foo do arquivo bar.s, para isso, basta usar o comando
\begin{verbatim}
(gdb) break bar.s:foo
\end{verbatim}

Observe que, caso haja apenas um arquivo fonte, o nome do arquivo é
desnecessário em ambos comandos acima.

Quando não se precisa mais de um breakpoint, é conveniente removê-lo. Para
remover um breakpoint, utiliza-se este comando:
\begin{verbatim}
(gdb) del NUMERO
\end{verbatim}

Onde NUMERO representa o número do breakpoint que se deseja remover.  Note que
cada breakpoint possui um número que o identifica, esse número aparece ao
criá-lo e sempre que o GDB pausa a execução nele.

%   \subsubsection{Watchpoints}
%   % TODO: conferir watchpoints em assembly

\subsection{Examinando dados}
Agora que o programa está pausado e como que podemos examinar seu status?

Existem diversas maneiras de se verificar a situação atual do programa, pode-se
investigar os valores nos registradores ou os valores de alguma variável
específica.% ou até mesmo examinar a memória.

\subsubsection{Registradores}
Quando se está depurando um programa em linguagem de montagem, uma informação
muito importante é qual o valor nos registradores. Por isso, ela é
consideravelmente simples de se conseguir, quando o GDB pausar num breakpoint,
para descobrir quais os valores que estão nos registradores, basta utilizar o
comando

\begin{verbatim}
(gdb) info registers
\end{verbatim}

Esse comando listará os registradores pelo nome seguidos de duas colunas, sendo
a primeira o valor que está no registrador no formato hexadecimal e na segunda o
mesmo valor em decimal.

\paragraph*{}
O comando {\tt info}, além de obter as informações dos registradores, também
pode ser usado para obter informações dos breakpoints que você criou, para isso,
é só utilizar {\tt info break}.

\subsubsection{Variável}
Também é possível verificar o valor de uma variável, para tal, deve-se utilizar
o comando {\tt print} que recebe como argumento o nome de uma variável, note que
esse nome de variável pode ser o nome do rótulo associado à essa variável.

Observe também que no argumento do {\tt print} pode-se usar diversos dos
operadores de expressões usados em C, com por exemplo, soma, multiplicação, o
operador unário {\tt \&}, que retorna o endereço de uma variável, até mesmo o
operador unário {\tt *} que considera o valor ao qual é aplicado como sendo o
endereço de uma variável. Também há a possibilidade de se utilizar typecasts
para os tipos comuns como {\tt int} e {\tt char}.

%   \subsubsection{Região de memória}
%   comando 'x'

\subsection{Continuando a execução}
Depois de pausar a execução e verificar o valor de suas variáveis... e agora?
Como voltar a executar o programa?

Há várias maneiras de se retomar a execução do programa, que podem ser passo a
passo (instrução por instrução) ou executando o programa até terminar ou
encontrar outro breakpoint.

\paragraph*{}
Para executar o programa passo a passo, existem dois comandos: o {\tt stepi} e
{\tt nexti}, a principal diferença entre os dois é que o {\tt stepi} (step
instruction) realmente executa tudo passo a passo até mesmo chamadas de funções,
já o {\tt nexti} (next instruction) executa instrução por instrução mas ao
encontrar uma chamada de função executa a função como se fosse uma única
instrução.

\paragraph*{}
Além de executar instruções por instruções, também é possível voltar a executar
o programa normalmente, para isso existe o comando {\tt continue} que retoma a
execução do programa normalmente ate que ele acabe ou encontre algum breakpoint.

\subsubsection{Mas onde eu estou agora?}
É muito fácil durante a depuração do programa perder noção de em qual trecho do
código você está, o GDB ao pausar a execução lhe mostra linha que ele irá
executar, mas as vezes isso não é o suficiente, para isso existe um comando
chamado {\tt list}, esse comando lista um trecho do código que está sendo
depurado. Se utilizado sem nenhum argumento, ele lista o trecho atual do
programa, porém é possível fazê-lo listar alguma função específica ou até mesmo
uma linha específica de algum arquivo, para isso, você deve utilizá-lo assim

\begin{verbatim}
(gdb) list [arquivo:]arg
\end{verbatim}

onde o {\tt arg} pode ser o número da linha ou o nome de uma função, note
que o argumento opcional ({\tt arquivo}) é o nome do arquivo do qual você deseja
listar um trecho.

% TODO: comentar do 'TUI'?

%   \subsection{Alterando o programa sendo executado}
%   \subsubsection{Alterando dados}
%   \subsubsection{Alterando o fluxo do programa}

\section{Abreviações de comandos}
Como existem diversos comandos que são repetidos diversas vezes, o GDB possui
alguns ``atalhos'' para os mesmo, esse atalho nada mais é do que o comando
abreviado, veja na tabela~\ref{tab:abrev} a lista das principais abreviações do
GDB.

\begin{table}[h]
    \centering
    \begin{tabular}{|l|c|}\hline
        Comando         & Abreviação\\\hline\hline
        {\tt run}       & {\tt r}\\\hline
        {\tt break}     & {\tt b}\\\hline
        {\tt info}      & {\tt i}\\\hline
        {\tt registers} & {\tt reg}\\\hline
        {\tt print}     & {\tt p}\\\hline
        {\tt stepi}     & {\tt si}\\\hline
        {\tt nexti}     & {\tt ni}\\\hline
        {\tt continue}  & {\tt c}\\\hline
        {\tt list}      & {\tt l}\\\hline
    \end{tabular}
    \caption{Abreviação de alguns comandos do GDB}\label{tab:abrev}
\end{table}

Além disso, no terminal do GDB, ao pressionar a tecla Enter sem digitar nada, o
GDB executa o último comando, então por exemplo, você está executando uma rotina
passo a passo (com o {\tt stepi}) e ao invés de digitar {\tt stepi} a cada
instrução, basta digitar o comando uma vez e apertar a tecla Enter sem digitar
nada.

\section{Mais informações}
O GDB é um programa bem complexo e é difícil decorar todos seus comandos e isso
não é esperado de ninguém, tanto que o próprio GDB possui comandos cuja função
são ajudar as pessoas a lembrarem de outros comandos.

Os comandos de ajuda são:
\begin{itemize}
    \item {\tt help}
    \item {\tt apropos}
\end{itemize}

Com esses dois comandos é possível relembrar a sintaxe de determinados comandos
e comandos relacionados com certas funcionalidades. O {\tt apropos} é utilizado
para buscar na descrição dos comandos existentes alguma palavra chave, por
exemplo, o comando:

\begin{verbatim}
(gdb) apropos breakpoint
\end{verbatim}

Esse comando listará todos os comandos relacionados com breakpoints e uma
descrição dos mesmos, nessa lista temos o comando {\tt break}, que para ver como
ele deve ser utilizado, basta usar o comando

\begin{verbatim}
(gdb) help break
\end{verbatim}

que listará a sintaxe do comando junto de uma explicação do significado de cada
argumento.

Além desses comandos, é muito fácil encontrar informações sobre o GDB pela
internet, um bom lugar para começar é na Documentação oficial
(\url{http://www.gnu.org/software/gdb}) em Inglês.
\end{document}
