#mc404abe 2013s1 gdb

Arquivos fonte do tutorial de como usar o gdb no ambiente ARM disponível para
as turmas de MC404 abe do primeiro semestre de 2013.

* * *
Tutorial escrito por Alexandre Medeiros.

Fortemente inspirado em:

- Palestra de gdb do Sergio Durigan Junior
- [Documentação do gdb](http://www.gnu.org/software/gdb/)
- Comandos `help` e `apropos` do gdb
